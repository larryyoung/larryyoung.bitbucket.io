//SLIDESHOW

//This defines the array of images and a reference for img width (pixels)

var imgArray = ['./img/bouncingBallsImg.png',
 								'./img/sweetshottopdecor.jpg',
								'./img/background3.jpg',
								'./img/jooklogo.png'];

var imgWidth = 300;
var i = 0;

//uses imgArray info to build a slide for imgs

function createSlide(i) {
	var newSlide = document.createElement('div');
	var newImg = document.createElement('img');
	newImg.src = imgArray[i];
	newSlide.setAttribute("id", "slide" + (i + 1))
	newSlide.setAttribute("class", "slide");
	var container = document.getElementById('slideContainer');
	newSlide.style.cssText = 'width:300px;height:100%;border-radius:3%;padding:5px';
	newSlide.appendChild(newImg);
	container.appendChild(newSlide);
}

function buildSlides() {
	for (imgArray.length > 0; i < imgArray.length; i++) {
		createSlide(i);
	}
}

buildSlides();

function setContainerWidth() {
	var containerWidth = (imgArray.length * imgWidth) + (10 * imgArray.length);
	document.getElementById('slideContainer').style.width = containerWidth + "px";
}

function setFlex() {
	var flexIndex = 100 / imgArray.length;
	$('.slide').css('flex', 'flexIndex + "%"');
}

setContainerWidth();
setFlex();

//just copy and paste new else if clause when adding project and increase/decrease i=x and/or nth(n)!


var i2 = 1;
function arrowHov() {
	if(i2 === imgArray.length) {
		$('#slideForwardArrow').css('opacity', '0.2');
		$('#slideBackArrow').css('opacity', '');
	} else if (i2 === 1) {
		$('#slideBackArrow').css('opacity', '0.2');
		$('#slideForwardArrow').css('opacity', '');
 } else {
	  $('#slideBackArrow').css('opacity', '');
    $('#slideForwardArrow').css('opacity', '');
  }
}
function slideShowForward() {
  for(i2<imgArray) {

  }
	if (i2 === 1) {
		$("#slideContainer div:first-child").fadeOut(100);
		$("#slideContainer div:nth-child(2)").fadeIn(100);
		$("#slideContainer div:nth-child(2)").css("opacity", "0.5");
		i2++;
	} else if (i2 === 2) {
		$("#slideContainer div:nth-child(2)").fadeOut(100);
		$("#slideContainer div:nth-child(3)").fadeIn(100);
		$("#slideContainer div:nth-child(3)").css("opacity", "0.5");
		i2++;
	} else if (i2 === 3) {
		$("#slideContainer div:nth-child(3)").fadeOut(100);
		$("#slideContainer div:nth-child(4)").fadeIn(100);
		$("#slideContainer div:nth-child(4)").css("opacity", "0.5");
		$("#slideForwardArrow").css("opacity", "0.2");
		$(".slideForwardArrowHover").off('mouseenter mouseleave');
		i2++;
	}
}

function slideShowBack() {
	if (i2 === 2) {
		$("#slideContainer div:nth-child(2)").fadeOut(100);
		$("#slideContainer div:first-child").fadeIn(100);
		$('#slideContainer').css("opacity", "0.5");
		i2--;
	} else if (i2 === 3) {
		$("#slideContainer div:nth-child(3)").fadeOut(100);
		$('#slideContainer').css("opacity", "0.5");
		$("#slideContainer div:nth-child(2)").fadeIn(100);
		i2--;
	}	else if (i2 === 4) {
			$("#slideContainer div:nth-child(4)").fadeOut(100);
			$('#slideContainer').css("opacity", "0.5");
			$("#slideContainer div:nth-child(3)").fadeIn(100);
			i2--;
	}
}

arrowHov();

//call with click function arrows

document.getElementById('slideForwardArrow').addEventListener('click', function() {
	slideShowForward();
	arrowHov();
});

document.getElementById('slideBackArrow').addEventListener('click', function() {
	slideShowBack();
	arrowHov();
});
