alert("JavaScript passed!!");

var namehead = ['L', 'a', 'r', 'r', 'y', ' ', 'Y', 'o', 'u', 'n', 'g'];

var clickLock = false; //lock to prevent users from clicking elements while animations are running

var activePage;

function fadeOutBioPage() {
	$('#bc1').fadeOut(1500);
	$('#biography').fadeOut(1500);
	$("#biohead").fadeOut(2000);
}
	//bc2 homeScrn
function fadeOutPortfolioPage() {
	$('#slideContainer').fadeOut(2000);
	$('#slideBackArrow').fadeOut(2000);
	$('#slideForwardArrow').fadeOut(2000);
	$('#porthead').fadeOut(2000);
}
//bc3 homeScrn
function fadeOutContactPage() {
	var bc3 = document.getElementById('bc3');
	$('#contactContainer').fadeOut(1500);
	$('#bc3').fadeOut(1500);
}



document.getElementById('bc1').addEventListener('click', function openBio() {
	if(clickLock) {
		return false;
	}
	clickLock = true;
	activePage = 'bioPage';
	$('#bc1').fadeOut(1500);
	$('#bc3').fadeOut(500);
	var pos = 33;
	let port = document.getElementById('portfolio');
	let to = win.clientHeight - port.clientHeight;
	$('#bc2').animate({
		top: to
	},
		2000,
		'easeOutBounce');
	setTimeout(bPFade, 2000)
	setTimeout(unlock, 4000)
	function bPFade() {
		document.getElementById('window').style.backgroundImage = "url('./img/background2.jpg')";
		$('#bc2').fadeOut(1000);
		$('#bc1').css('float', 'left');
		$("#backbutton").fadeIn(2000);
		$("#biohead").fadeIn(2000);
		$("#bc1").removeClass("bchover");
		$("#bc1").removeClass("butcontainer");
		$('.label').css('opacity', '0');
		//$("#bc1").removeClass("bcpos");
		//$("#bio").fadeOut(1000);
		document.getElementById('bio').style.backgroundImage = "url('./img/biophoto.jpg')";
		document.getElementById('bio').style.backgroundSize = "100% 100%";
		document.getElementById('bc1').style.borderRadius = '50%';
		var bc1 = document.getElementById('bc1');
		document.getElementById('bc1').style.marginLeft = '17%';
		document.getElementById('bc1').style.marginTop = '4%';
		bc1.style.height = '300px';
		bc1.style.width = '25%';
		$('#bc1').fadeIn(3000);
		$('#biography').fadeIn(4000);
	}
	function unlock() {
		clickLock = false;
	}
});

let bc2 = document.getElementById('bc2');
let win = document.getElementById('window');

document.getElementById('bc2').addEventListener('click', function() {
	if(clickLock) {
		return false;
	}
	clickLock = true;
	activePage = 'portPage';
	$('#bc1').fadeOut(1500);
	$('#bc3').fadeOut(1500);
	$("#bc2").removeClass("bchover");
	let bc2 = document.getElementById('bc2');
	let port = document.getElementById('portfolio');
	let to = win.clientHeight - port.clientHeight;
	$('#bc2').animate({
		top: to
	},
		2000,
		'easeOutBounce');
	function portFadeIn() {
		$('#porthead').fadeIn(2000);
		var element = document.getElementById('window');
		element.style.background = "url('./img/background3.jpg')";
		$("#backbutton").fadeIn(2000);
		$('#bc2').fadeOut(1750);
		$('#slideBackArrow').fadeIn(2000);
		$('#slideForwardArrow').fadeIn(2000);
		$('#slideContainer').css('display', 'flex');
		$('#slideContainer').fadeIn(2000);
		clickLock = false;
	}
	setTimeout(portFadeIn, 2300);
});

var clicks = 0;

document.getElementById('bc3').addEventListener('click', function() {
	if(clickLock) {
		return false;
	}
	clickLock = true;
	activePage = 'conPage';
	setTimeout(unlockbc3, 4000)
	function unlockbc3() {
		clickLock = false;
	}
	var element = document.getElementById('window');
	element.style.background = "url('./img/background4.jpg')";
	if(clicks === 0) {
		let port = document.getElementById('portfolio');
		let to = win.clientHeight - port.clientHeight;
		$('#bc2').animate({
			top: to
			},
			2000,
			'easeOutBounce');
		$('#bc3').animate({
			top: '35%',
			left: '37%',
				},
			4000,
			'elasout');
		$('#bc1').fadeOut(1500);
		$('#bc2').fadeOut(3000);
		$("#bc3").removeClass("bchover");
		$("#bc2").removeClass("bchover");
		$("#bc3").css('transform', 'scale(1.5, 1.5)');
		$("#bc3").addClass("contactHeight");
		var contactContainer = document.createElement('div');
		contactContainer.setAttribute('id', 'contactContainer')
		contactContainer.style.cssText = 'position:absolute;width:15%;top:33px;left:22%; z-index:2';
		var phoneImg = document.createElement('img');
		var phoneNumber = document.createElement('p');
		var emailImg = document.createElement('img');
		var emailAddress = document.createElement('p');
		//newSlide.style.cssText = 'width:300px;height:100%;border-radius:3%;padding:5px';
		var gitHubImg = document.createElement('img');
		var gitHubAddress = document.createElement('p');
		//newSlide.style.cssText = 'width:300px;height:100%;border-radius:3%;padding:5px';
		var bitBucketImg = document.createElement('img');
		var bitBucketAddress = document.createElement('p');
		//newSlide.style.cssText = 'display:none;width:300px;height:100%;border-radius:3%;padding:5px';
		var contact = document.getElementById('contact');
		contact.appendChild(contactContainer);
		contactContainer.appendChild(phoneImg);
		contactContainer.appendChild(phoneNumber);
		contactContainer.appendChild(emailImg);
		contactContainer.appendChild(emailAddress);
		contactContainer.appendChild(gitHubImg);
		contactContainer.appendChild(gitHubAddress);
		contactContainer.appendChild(bitBucketImg);
		contactContainer.appendChild(bitBucketAddress);
		phoneImg.setAttribute("id", "phoneImg");
		phoneImg.setAttribute("class", "icon");
		phoneNumber.setAttribute("id", "phoneNumber");
		emailImg.setAttribute("id", "emailImg");
		emailImg.setAttribute("class", "icon");
		emailAddress.setAttribute("id", "emailAddress");
		gitHubImg.setAttribute("id", "gitHubImg");
		gitHubImg.setAttribute("class", "icon");
		gitHubAddress.setAttribute("id", "gitHubAddress");
		bitBucketImg.setAttribute("id", "bitBucketImg");
		bitBucketImg.setAttribute("class", "icon");
		bitBucketAddress.setAttribute("id", "bitBucketAddress");
		phoneImg.src = './img/icon.png';
		emailImg.src = './img/email-filled-closed-envelope.png';
		gitHubImg.src = './img/github-logo-icon-16174.png';
		bitBucketImg.src = './img/bitbucket.png';
		$('#contactContainer').fadeIn(4000);
		var parentDOM = document.getElementById("contactContainer");
		var iconTest = parentDOM.getElementsByClassName("icon"); // a list of matching elements, *not* the element itself
		console.log(iconTest); //test class name array
		var icon = parentDOM.getElementsByClassName('icon');
		function addIconStyles(){

			for(var i=0; i<4; i++) {
					icon[i].style.width = '40%';
					//icon[i].style.position: relative;
					icon[i].style.textAlign = 'left';
					icon[i].style.display = 'none';
					icon[i].style.opacity = '0.75';
			}
		}
		addIconStyles();
		$('#contactContainer').css('text-align', 'left');
		$('#contitle').css('display', 'none');
		$('#contact').css('padding-bottom', '100%');
		var phoneNumberStyle = document.getElementById("phoneNumber");
		document.getElementById("phoneNumber").innerHTML = "1-660-428-2313";
		var emailAddressStyle = document.getElementById("emailAddress");
		document.getElementById("emailAddress").innerHTML = "L89471@gmail.com";
		var gitHubAddressStyle = document.getElementById('gitHubAddress');
		document.getElementById("gitHubAddress").innerHTML = "GitHub Repositories";
		var bitBucketAddressStyle = document.getElementById('bitBucketAddress');
		document.getElementById("bitBucketAddress").innerHTML = "BitBucket Website";
		phoneNumberStyle.style.cssText = 'display:none;position:relative;opacity:0.7;min-width:150px;left:90%;font-family:"CenturyGothic";font-size:1.0vw';
		emailAddressStyle.style.cssText = 'display:none;position:relative;opacity:0.7;min-width:150px;left:90%;font-family:"CenturyGothic";font-size:1.0vw';
		gitHubAddressStyle.style.cssText = 'display:none;position:relative;opacity:0.7;min-width:150px;left:90%;font-family:"CenturyGothic";font-size:1.0vw';
		bitBucketAddressStyle.style.cssText = 'display:none;position:relative;opacity:0.7;min-width:150px;left:90%;font-family:"CenturyGothic";font-size:1.0vw';
		$('.icon').fadeIn(9000);
		$('#phoneNumber').fadeIn(9000);
		$('#emailAddress').fadeIn(9000);
		$('#gitHubAddress').fadeIn(9000);
		$('#bitBucketAddress').fadeIn(9000);
		$('#backbutton').fadeIn(9000);
	} else {
		$('#bc1').fadeOut(1500);
		$('#bc2').fadeOut(3000);
		$("#bc3").css('transform', 'scale(1.5, 1.5)');
		$("#bc3").removeClass("bchover");
		$("#bc2").removeClass("bchover");
		$('#contactContainer').fadeIn(9000);
		$('.icon').fadeIn(9000);
		$('#phoneNumber').fadeIn(9000);
		$('#emailAddress').fadeIn(9000);
		$('#gitHubAddress').fadeIn(9000);
		$('#bitBucketAddress').fadeIn(9000);
		$('#backbutton').fadeIn(9000);
		let port = document.getElementById('portfolio');
		let to = win.clientHeight - port.clientHeight;
		$('#bc2').animate({
			top: to
		},
			2000,
			'easeOutBounce');
		$('#bc3').animate({
			top: '35%',
			left: '37%',
				},
			4000,
			'elasout');
		$('#contactContainer').css('text-align', 'left');
		$('#contitle').css('display', 'none');
		$('#contact').css('padding-bottom', '100%');
	}
	clicks++;
});
//SLIDESHOW

//This defines the array of images and a reference for img width (pixels)

var imgArray = ['./img/bouncingBallsImg.png',
 								'./img/sweetshottopdecor.jpg',
								'./img/background3.jpg',
								'./img/jooklogo.png'];

var imgWidth = 300;
var i = 0;

//uses imgArray info to build a slide for imgs

function createSlide(i) {
	var newSlide = document.createElement('div');
	var newImg = document.createElement('img');
	newImg.src = imgArray[i];
	newSlide.setAttribute("id", "slide" + (i + 1))
	newSlide.setAttribute("class", "slide");
	var container = document.getElementById('slideContainer');
	newSlide.style.cssText = 'width:300px;height:100%;border-radius:3%;padding:5px';
	newSlide.appendChild(newImg);
	container.appendChild(newSlide);
}

function buildSlides() {
	for (imgArray.length > 0; i < imgArray.length; i++) {
		createSlide(i);
	}
}

buildSlides();

function setContainerWidth() {
	var containerWidth = (imgArray.length * imgWidth) + (10 * imgArray.length);
	document.getElementById('slideContainer').style.width = containerWidth + "px";
}

function setFlex() {
	var flexIndex = 100 / imgArray.length;
	$('.slide').css('flex', 'flexIndex + "%"');
}

setContainerWidth();
setFlex();

//just copy and paste new else if clause when adding project and increase/decrease i=x and/or nth(n)!


var i2 = 1;
function arrowHov() {
	if(i2 === imgArray.length) {
		$('#slideForwardArrow').css('opacity', '0.2');
		$('#slideBackArrow').css('opacity', '');
	} else if (i2 === 1) {
		$('#slideBackArrow').css('opacity', '0.2');
		$('#slideForwardArrow').css('opacity', '');
 } else {
	  $('#slideBackArrow').css('opacity', '');
    $('#slideForwardArrow').css('opacity', '');
  }
}
function slideShowForward() {
	if (i2 === 1) {
		$("#slideContainer div:first-child").fadeOut(100);
		$("#slideContainer div:nth-child(2)").fadeIn(100);
		$("#slideContainer div:nth-child(2)").css("opacity", "0.5");
		i2++;
	} else if (i2 === 2) {
		$("#slideContainer div:nth-child(2)").fadeOut(100);
		$("#slideContainer div:nth-child(3)").fadeIn(100);
		$("#slideContainer div:nth-child(3)").css("opacity", "0.5");
		i2++;
	} else if (i2 === 3) {
		$("#slideContainer div:nth-child(3)").fadeOut(100);
		$("#slideContainer div:nth-child(4)").fadeIn(100);
		$("#slideContainer div:nth-child(4)").css("opacity", "0.5");
		$("#slideForwardArrow").css("opacity", "0.2");
		$(".slideForwardArrowHover").off('mouseenter mouseleave');
		i2++;
	}
}

function slideShowBack() {
	if (i2 === 2) {
		$("#slideContainer div:nth-child(2)").fadeOut(100);
		$("#slideContainer div:first-child").fadeIn(100);
		$('#slideContainer').css("opacity", "0.5");
		i2--;
	} else if (i2 === 3) {
		$("#slideContainer div:nth-child(3)").fadeOut(100);
		$('#slideContainer').css("opacity", "0.5");
		$("#slideContainer div:nth-child(2)").fadeIn(100);
		i2--;
	}	else if (i2 === 4) {
			$("#slideContainer div:nth-child(4)").fadeOut(100);
			$('#slideContainer').css("opacity", "0.5");
			$("#slideContainer div:nth-child(3)").fadeIn(100);
			i2--;
	}
}

arrowHov();

document.getElementById('backbutton').addEventListener('click', function() {
	if(clickLock) {
		return false;
	}
	clickLock = true;
	$("#backbutton").fadeOut(500);
	//bc1 homeScrn
	function executeFadeOut() {
		if (activePage === 'bioPage') {
			fadeOutBioPage();
		} else if (activePage === 'portPage') {
			fadeOutPortfolioPage();
		} else if (activePage === 'conPage') {
			fadeOutContactPage();
		}
	}
	executeFadeOut();
	setTimeout(homeScrn, '2500');
	function homeScrn() {
		$('#contact').css("padding-bottom", "40%");
		$("#bc2").addClass("bchover");
		$("#bc3").addClass("bchover");
		$('.label').css('opacity', '1');
		$("#bc1").addClass("bchover");
		$("#bc1").addClass("butcontainer");
		var bc1 = document.getElementById('bc1');
		bc1.style.width = '20%';
		bc1.style.height = '300px';
		bc1.style.width = '20%';
		bc1.style.height = '300px';
		document.getElementById('bio').style.backgroundImage = "none";
		document.getElementById('bio').style.backgroundSize = "100% 100%";
		document.getElementById('bc1').style.marginTop = '0';
		document.getElementById('bc1').style.marginLeft = '0';
		document.getElementById('bc2').style.top = '37%';
		document.getElementById('window').style.backgroundImage = "url('./img/background.jpg')";
		document.getElementById('bc3').style.cssText = 'left:65%;top:25%';
		$('#bc3').css('position', 'absolute');
		$('#contitle').fadeIn(1500);
		setTimeout(fadeInHome, 1000)
		function fadeInHome() {
			$('#bc1').fadeIn(1500);
			$('#bc2').fadeIn(1500);
			$('#bc3').fadeIn(1500);
		}
	}

	clickLock = false;
});

document.getElementById('slideForwardArrow').addEventListener('click', function() {
	slideShowForward();
	arrowHov();
});

document.getElementById('slideBackArrow').addEventListener('click', function() {
	slideShowBack();
	arrowHov();
});

$(document).ready(function fadeIn(){
	$('#window').fadeIn(3000);
	$('#bc1').fadeIn(2400);
	$('#bc2').fadeIn(2600);
	$('#bc3').fadeIn(3000);
});
